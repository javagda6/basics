package com.amen.sda.Zadanie36;

import java.time.LocalDateTime;

public class Record {
    private LocalDateTime czasUtworzenia;
    private long timeout;
    //    private LocalDateTime localDateTimePrzedawnione;/* = LocalDateTime.now().plusSeconds(30);*/
    private int id;
    private String name;

    public Record(LocalDateTime localDateTime, int id, String name, long timeout) {
        this.czasUtworzenia = localDateTime;
        this.id = id;
        this.name = name;

        this.timeout = timeout;
    }

    public LocalDateTime getCzasPrzedawnienia() {
        // timeout jest w milisekundach, zamieniam go na sekundy (stąd /1000)
        return czasUtworzenia.plusSeconds(timeout / 1000);
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    //
//    public Record(LocalDateTime czasUtworzenia, int id, String name, long timeout) {
//        this.czasUtworzenia = czasUtworzenia;
//        this.id = id;
//        this.name = name;
//
//        this.localDateTimePrzedawnione = czasUtworzenia.plusSeconds(timeout / 1000);
//    }

    public LocalDateTime getCzasUtworzenia() {
        return czasUtworzenia;
    }

    public void setCzasUtworzenia(LocalDateTime czasUtworzenia) {
        this.czasUtworzenia = czasUtworzenia;
    }

//    public LocalDateTime getLocalDateTimePrzedawnione() {
//        return localDateTimePrzedawnione;
//    }

//    public void setLocalDateTimePrzedawnione(LocalDateTime localDateTimePrzedawnione) {
//        this.localDateTimePrzedawnione = localDateTimePrzedawnione;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}