package com.amen.sda.Zadanie35;

public class Client {
    private TypSprawy sprawa;
    private String pesel;

    public Client(TypSprawy sprawa, String pesel) {
        this.sprawa = sprawa;
        this.pesel = pesel;
    }

    public TypSprawy getSprawa() {
        return sprawa;
    }

    public void setSprawa(TypSprawy sprawa) {
        this.sprawa = sprawa;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Client{" +
                "sprawa=" + sprawa +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
