package com.amen.sda.Zadanie35;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Department department = new Department();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            String trimmedAndLower = line.toLowerCase().trim();
            String[] words = trimmedAndLower.split(" ");

            // dodaj_biuro
            // words[0] = dodaj_biuro

            // obsluz 123124 REGISTER 0
            // words[0] = 'obsluz'
            // words[1] = '123124'
            // words[2] = 'REGISTER'
            // words[3] = '0'

            if (words[0].equals("dodaj_biuro")) {
//                department.addOffice();
                System.out.println("Dodaje biuro!");
                department.addOffice(new Office());
            } else if (words[0].equals("obsluz")) {
                String pesel = words[1];
                String typSprawy = words[2];
                String nrPokoju = words[3];

                try {
                    // Typ sprawy ze string na enum
                    TypSprawy typ = TypSprawy.valueOf(typSprawy);

                    // nr pokoju ze string na liczbe
                    int nr_pokoju = Integer.parseInt(nrPokoju);

                    // Tworze klienta (przekazuje mu sprawe i pesel)
                    Client client = new Client(typ, pesel);

                    // pobieram pokoj
                    Office office = department.getOffice(nr_pokoju);

                    // przekazuje klienta do obsługi do pokoju
                    office.handle(client);
                } catch (NumberFormatException nfe) {
                    System.out.println("Niepoprawny format liczbowy");
                } catch (IllegalArgumentException iae) {
                    System.out.println("Zły argument");
                }
            } else if (words[0].equals("quit")) {
                break;
            }
        }
    }
}
