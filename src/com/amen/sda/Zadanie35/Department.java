package com.amen.sda.Zadanie35;

import java.util.ArrayList;
import java.util.List;

public class Department {
    private List<Office> offices = new ArrayList<>();

    // opcja 1
    public void addOffice(Office office) {
        offices.add(office);
    }

    // opcja 2
    public void addOffice() {
        offices.add(new Office());
    }

    public Office getOffice(int indeks){
        // dodaje zabezpieczenie pobrania nieistniejacego biura
        if(offices.get(indeks) == null){
            throw new IllegalArgumentException();
        }
        return offices.get(indeks);
    }
}
